﻿using System;
namespace Zoology
{
	public class Animal
	{
		public int Legs { get; }
		private string Sound;
		public Boolean IsWarmblooded { get; }
		public double Weight { get; }
		public Animal()
		{
			Weight = 10;
			Sound = "HONK!";
			IsWarmblooded = true;
			Legs = 4;
		}

		public Animal(int legs, Boolean warmblooded, double weight)
		{
			Legs = legs;
			IsWarmblooded = warmblooded;
			Weight = weight;
			Sound = "HONK!";
		}

		public Animal(int legs, Boolean warmblooded, double weight, string sound)
		{
			Legs = legs;
			IsWarmblooded = warmblooded;
			Weight = weight;
			Sound = sound;
		}


		public string MakeSound()
		{
			return Sound;
		}
	}
}

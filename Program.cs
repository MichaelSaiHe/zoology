﻿using System;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal goose = new Animal();
            Animal bigGoose = new Animal(2, true, 20);
            Animal fox = new Animal(4, true, 37.2 , "huh?");
            Console.WriteLine(goose.MakeSound());
            Console.WriteLine(fox.MakeSound());
        }
    }
}
